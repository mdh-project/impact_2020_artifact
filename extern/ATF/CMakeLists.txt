cmake_minimum_required(VERSION 2.8.11)
project(ATF)

set(CMAKE_CXX_STANDARD 14)
set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -pthread -fPIC")

# OpenCL
find_package(OpenCL REQUIRED)
include_directories(${OpenCL_INCLUDE_DIR})
link_directories(${OpenCL_LIBRARY})

include_directories(include)

file(GLOB_RECURSE HEADER_FILES include/*.hpp)
file(GLOB_RECURSE SOURCE_FILES src/abort_conditions.cpp src/ocl_wrapper.cpp src/op_wrapper.cpp src/process_wrapper_helper.cpp src/tp_value.cpp src/tp_value_node.cpp src/tuner_with_constraints.cpp src/tuner_without_constraints.cpp src/value_type.cpp)

add_library(atf SHARED ${SOURCE_FILES} ${HEADER_FILES})
get_filename_component(ATF_INCLUDE_DIR_ABSOLUTE . ABSOLUTE)
set(ATF_INCLUDE_DIRS ${ATF_INCLUDE_DIR_ABSOLUTE} ${OpenCL_INCLUDE_DIR} ${PYTHON_INCLUDE_DIR} ${ARGPARSE_INCLUDE_DIR} PARENT_SCOPE)