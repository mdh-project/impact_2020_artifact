//
//  auc_bandit.hpp
//  new_atf_lib
//

#ifndef auc_bandit_hpp
#define auc_bandit_hpp

#include <deque>
#include <algorithm>
#include <numeric>
#include <random>

#include "tuner_with_constraints.hpp"
#include "tuner_without_constraints.hpp"



namespace atf
{

    template< typename T = tuner_with_constraints>
    class auc_bandit_class : public T
    {
    public:

        static constexpr double DEFAULT_C = 0.05;

        static constexpr size_t DEFAULT_WINDOW_SIZE = 500;

        struct history_entry {
          size_t technique_index;
          bool   cost_has_improved;
        };


        template< typename... Ts >
        explicit auc_bandit_class( const std::vector<std::reference_wrapper<T>>& techniques, double c = DEFAULT_C, size_t window_size = DEFAULT_WINDOW_SIZE, Ts... params )
                : T(  params... ), _c( c ), _window_size( window_size ), _rand_generator( random_seed() ), _techniques( techniques ), _current_technique_index( 0 ),
                _current_best_cost( std::numeric_limits<unsigned long long>::max() ), _history(), _uses( techniques.size() ), _raw_auc( techniques.size() ), _decay( techniques.size() )
        {}


        void initialize( const search_space& search_space ) override
        {
          for( auto& technique : _techniques )
            technique.get().initialize( search_space );
        }


        configuration get_next_config() override
        {
          _current_technique_index  = get_best_technique_index();

          std::cout << "Technique " << _current_technique_index << std::endl;

          return _techniques[ _current_technique_index ].get().get_next_config();
        }


        void report_result( const unsigned long long& result ) override
        {
          _techniques[ _current_technique_index ].get().report_result( result );

          bool cost_has_improved = false;
          if( result < _current_best_cost )
          {
            _current_best_cost = result;
            cost_has_improved = true;
          }

          history_push( _current_technique_index, cost_has_improved );
        }


        void finalize() override
        {
          for( auto& technique : _techniques )
            technique.get().finalize();
        }


    private:

        double _c;
        size_t _window_size;

        std::default_random_engine _rand_generator;

        std::vector<std::reference_wrapper<T>> _techniques;
        size_t                                 _current_technique_index;
        unsigned long long                                 _current_best_cost;

        std::deque<history_entry>       _history;
        std::vector<unsigned long long> _uses;
        std::vector<unsigned long long> _raw_auc;
        std::vector<unsigned long long> _decay;


        size_t random_seed() const
        {
          return std::chrono::system_clock::now().time_since_epoch().count();
        }


        void history_push( size_t technique_index, bool cost_has_improved )
        {
          // remove oldest entry if history is full
          if( _history.size() == _window_size )
          {
            auto oldest_technique = _history.front();

            _uses[ oldest_technique.technique_index ]--;
            _raw_auc[ oldest_technique.technique_index ] -= _decay[ oldest_technique.technique_index ];
            if( oldest_technique.cost_has_improved )
              _decay[ oldest_technique.technique_index ]--;

            _history.pop_front();
          }

          _uses[ technique_index ]++;
          if( cost_has_improved )
          {
            _raw_auc[ technique_index ] += _uses[ technique_index ];
            _decay[ technique_index ]   += 1;
          }

          _history.push_back( { technique_index, cost_has_improved } );
        }


        double calculate_auc( size_t technique_index )
        {
          auto uses = _uses[ technique_index ];
          if( uses > 0 )
            return _raw_auc[ technique_index ] * 2.0 / ( uses * ( uses + 1.0 ) );
          else
            return 0.0;
        }


        double calculate_exploration_value( size_t technique_index )
        {
          if( _uses[ technique_index ] > 0 )
            return std::sqrt( 2.0 * std::log2( _history.size() ) / _uses[ technique_index ] );
          else
            return std::numeric_limits<double>::infinity();
        }


        double calculate_score( size_t technique_index )
        {
          return calculate_auc( technique_index ) + _c * calculate_exploration_value( technique_index );
        }


        size_t get_best_technique_index()
        {
          std::vector<size_t> indices( _techniques.size() );
          std::iota( indices.begin(), indices.end(), 0 );

          // randomize order of techniques with equal score
          std::shuffle( indices.begin(), indices.end(), _rand_generator );

          return *std::max_element( indices.begin(), indices.end(), [&]( size_t i_1, size_t i_2 ) {
            return calculate_score( i_1 ) < calculate_score( i_2 );
          } );
        }



        std::string display_string() const {
            return "AUC Bandit";
        }
    };


    template< typename... Ts >
    auto auc_bandit( const std::vector<std::reference_wrapper<atf::tuner_with_constraints>>& techniques, Ts... args )
    {
      return auc_bandit_class<>{ techniques, auc_bandit_class<>::DEFAULT_C, auc_bandit_class<>::DEFAULT_WINDOW_SIZE, args... };
    }


    template< typename... Ts >
    auto auc_bandit( const std::vector<std::reference_wrapper<atf::tuner_with_constraints>>& techniques, double c, size_t window_size, Ts... args )
    {
      return auc_bandit_class<>{ techniques, c, window_size, args... };
    }


    template< typename T, typename... Ts >
    auto auc_bandit( const std::vector<std::reference_wrapper<T>>& techniques, Ts... args )
    {
      return auc_bandit_class<T>{ techniques, auc_bandit_class<T>::DEFAULT_C, auc_bandit_class<T>::DEFAULT_WINDOW_SIZE, args... };
    }


    template< typename T, typename... Ts >
    auto auc_bandit( const std::vector<std::reference_wrapper<T>>& techniques, double c, size_t window_size, Ts... args )
    {
      return auc_bandit_class<T>{ techniques, c, window_size, args... };
    }


} // namespace "atf"



#endif /* auc_bandit_hpp */
