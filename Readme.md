# md_poly: A Performance-Portable Polyhedral Compiler Based on Multi-Dimensional Homomorphisms

This artifact contains the workflow to reproduce the results shown in the work-in-progress paper *md_poly: A Performance-Portable Polyhedral Compiler Based on Multi-Dimensional Homomorphisms* accepted for publication at the [10th International Workshop on Polyhedral Compilation Techniques (2020)](http://impact.gforge.inria.fr/impact2020/). The user is invited to perform the steps described below.

## Software Requirements

- an OpenCL 1.2 driver and runtime environment
- CMake 2.8.11 or higher
- a compiler supporting C++14 or higher

## Workflow

1. Clone the artifact files:

   `$ git clone https://gitlab.com/mdh-project/impact_2020_artifact.git`
   
2. Change into the artifact directory:

   `$ cd impact_2020_artifact`
   
3. Run the experiments:

   `$ ./run_experiments.sh`
   
   **Note:** In case you have multiple OpenCL platforms and devices installed, you can edit the variables `CPU_OCL_PLATFORM` and `CPU_OCL_DEVICE` in line 14 and 15 of `run_experiments.sh`, as well as `GPU_OCL_PLATFORM` and `GPU_OCL_DEVICE` in line 51 and 52.