#include <iostream>
#include <cstdlib>
#include <atf.h>

#include "include/process_wrapper_helper.hpp"
using atf::cf::WEIGHTS_33;

void bench(size_t platform_id, size_t device_id, atf::cf::device_info &device_info,
           size_t N, size_t K, size_t P, size_t Q, size_t C,
           const std::vector<float> &in, const std::vector<struct WEIGHTS_33> &weights, const std::vector<float> &out) {
    // set configuration for kernel execution
    auto CACHE_L_CB          = atf::tp("CACHE_L_CB",          {0});
    auto CACHE_P_CB          = atf::tp("CACHE_P_CB",          {1});
    auto G_CB_RES_DEST_LEVEL = atf::tp("G_CB_RES_DEST_LEVEL", {2});
    auto L_CB_RES_DEST_LEVEL = atf::tp("L_CB_RES_DEST_LEVEL", {0});
    auto P_CB_RES_DEST_LEVEL = atf::tp("P_CB_RES_DEST_LEVEL", {0});
    auto INPUT_SIZE_L_1      = atf::tp("INPUT_SIZE_L_1",      {1});
    auto L_CB_SIZE_L_1       = atf::tp("L_CB_SIZE_L_1",       {1});
    auto P_CB_SIZE_L_1       = atf::tp("P_CB_SIZE_L_1",       {1});
    auto OCL_DIM_L_1         = atf::tp("OCL_DIM_L_1",         {4});
    auto NUM_WG_L_1          = atf::tp("NUM_WG_L_1",          {1});
    auto NUM_WI_L_1          = atf::tp("NUM_WI_L_1",          {1});
    auto INPUT_SIZE_L_2      = atf::tp("INPUT_SIZE_L_2",      {512});
    auto L_CB_SIZE_L_2       = atf::tp("L_CB_SIZE_L_2",       {8});
    auto P_CB_SIZE_L_2       = atf::tp("P_CB_SIZE_L_2",       {4});
    auto OCL_DIM_L_2         = atf::tp("OCL_DIM_L_2",         {0});
    auto NUM_WG_L_2          = atf::tp("NUM_WG_L_2",          {128});
    auto NUM_WI_L_2          = atf::tp("NUM_WI_L_2",          {2});
    auto INPUT_SIZE_L_3      = atf::tp("INPUT_SIZE_L_3",      {5});
    auto L_CB_SIZE_L_3       = atf::tp("L_CB_SIZE_L_3",       {4});
    auto P_CB_SIZE_L_3       = atf::tp("P_CB_SIZE_L_3",       {1});
    auto OCL_DIM_L_3         = atf::tp("OCL_DIM_L_3",         {1});
    auto NUM_WG_L_3          = atf::tp("NUM_WG_L_3",          {2});
    auto NUM_WI_L_3          = atf::tp("NUM_WI_L_3",          {4});
    auto INPUT_SIZE_L_4      = atf::tp("INPUT_SIZE_L_4",      {5});
    auto L_CB_SIZE_L_4       = atf::tp("L_CB_SIZE_L_4",       {4});
    auto P_CB_SIZE_L_4       = atf::tp("P_CB_SIZE_L_4",       {4});
    auto OCL_DIM_L_4         = atf::tp("OCL_DIM_L_4",         {3});
    auto NUM_WG_L_4          = atf::tp("NUM_WG_L_4",          {2});
    auto NUM_WI_L_4          = atf::tp("NUM_WI_L_4",          {4});
    auto INPUT_SIZE_R_1      = atf::tp("INPUT_SIZE_R_1",      {512});
    auto L_CB_SIZE_R_1       = atf::tp("L_CB_SIZE_R_1",       {128});
    auto P_CB_SIZE_R_1       = atf::tp("P_CB_SIZE_R_1",       {4});
    auto OCL_DIM_R_1         = atf::tp("OCL_DIM_R_1",         {2});
    auto NUM_WG_R_1          = atf::tp("NUM_WG_R_1",          {1});
    auto NUM_WI_R_1          = atf::tp("NUM_WI_R_1",          {1});
    auto L_REDUCTION         = atf::tp("L_REDUCTION",         {1});
    auto P_WRITE_BACK        = atf::tp("P_WRITE_BACK",        {0});
    auto L_WRITE_BACK        = atf::tp("L_WRITE_BACK",        {4});
    auto tuner = atf::exhaustive();
    tuner(CACHE_L_CB)(CACHE_P_CB)(G_CB_RES_DEST_LEVEL, L_CB_RES_DEST_LEVEL, P_CB_RES_DEST_LEVEL)
            (OCL_DIM_L_1, OCL_DIM_L_2, OCL_DIM_L_3, OCL_DIM_L_4, OCL_DIM_R_1)
            (INPUT_SIZE_L_1, L_CB_SIZE_L_1, P_CB_SIZE_L_1, NUM_WG_L_1, NUM_WI_L_1)
            (INPUT_SIZE_L_2, L_CB_SIZE_L_2, P_CB_SIZE_L_2, NUM_WG_L_2, NUM_WI_L_2)
            (INPUT_SIZE_L_3, L_CB_SIZE_L_3, P_CB_SIZE_L_3, NUM_WG_L_3, NUM_WI_L_3)
            (INPUT_SIZE_L_4, L_CB_SIZE_L_4, P_CB_SIZE_L_4, NUM_WG_L_4, NUM_WI_L_4)
            (INPUT_SIZE_R_1, L_CB_SIZE_R_1, P_CB_SIZE_R_1, NUM_WG_R_1, NUM_WI_R_1)
            (L_REDUCTION)(P_WRITE_BACK)(L_WRITE_BACK);

    // prepare kernel
    auto is_valid = [&](atf::configuration &config) -> bool {
        return true;
    };
    size_t res_g_l_size = N * K * P * Q * sizeof(float);
    std::vector<float> int_res(res_g_l_size / sizeof(float)); for (int i = 0; i < int_res.size(); ++i) int_res[i] = 0;
    auto res_g_size =
            [&](unsigned int kernel, atf::configuration &config) -> size_t {
                size_t size = res_g_l_size;
                if (kernel == 1) {
                    if (config[G_CB_RES_DEST_LEVEL.name()].value().int_val() == 2) {
                        size *= config[NUM_WG_R_1.name()].value().int_val();
                    }
                    if (config[L_CB_RES_DEST_LEVEL.name()].value().int_val() == 2) {
                        size *= config[NUM_WI_R_1.name()].value().int_val();
                    }
                } else {
                    if (config[L_CB_RES_DEST_LEVEL.name()].value().int_val() == 2) {
                        size *= config[NUM_WI_R_1.name()].value().int_val();
                    }
                }
                if (config[P_CB_RES_DEST_LEVEL.name()].value().int_val() == 2) {
                    size *= 1;
                }
                return size;
            };
    auto int_res_size =
            [&](atf::configuration &config) -> size_t {
                size_t size = res_g_l_size * config[NUM_WG_R_1.name()].value().int_val();
                return size;
            };
    auto needs_second_kernel =
            [&](atf::configuration &config) -> bool {
                return config[NUM_WG_R_1.name()].value().int_val() > 1;
            };
    auto gs_1 = atf::cf::GS(
              (OCL_DIM_L_1 == 0) * (NUM_WG_L_1) * (NUM_WI_L_1)
            + (OCL_DIM_L_2 == 0) * (NUM_WG_L_2) * (NUM_WI_L_2)
            + (OCL_DIM_L_3 == 0) * (NUM_WG_L_3) * (NUM_WI_L_3)
            + (OCL_DIM_L_4 == 0) * (NUM_WG_L_4) * (NUM_WI_L_4)
            + (OCL_DIM_R_1 == 0) * (NUM_WG_R_1) * (NUM_WI_R_1),

              (OCL_DIM_L_1 == 1) * (NUM_WG_L_1) * (NUM_WI_L_1)
            + (OCL_DIM_L_2 == 1) * (NUM_WG_L_2) * (NUM_WI_L_2)
            + (OCL_DIM_L_3 == 1) * (NUM_WG_L_3) * (NUM_WI_L_3)
            + (OCL_DIM_L_4 == 1) * (NUM_WG_L_4) * (NUM_WI_L_4)
            + (OCL_DIM_R_1 == 1) * (NUM_WG_R_1) * (NUM_WI_R_1),

              atf::max((OCL_DIM_L_1 >= 2) * (NUM_WG_L_1) * (NUM_WI_L_1), 1)
            * atf::max((OCL_DIM_L_2 >= 2) * (NUM_WG_L_2) * (NUM_WI_L_2), 1)
            * atf::max((OCL_DIM_L_3 >= 2) * (NUM_WG_L_3) * (NUM_WI_L_3), 1)
            * atf::max((OCL_DIM_L_4 >= 2) * (NUM_WG_L_4) * (NUM_WI_L_4), 1)
            * atf::max((OCL_DIM_R_1 >= 2) * (NUM_WG_R_1) * (NUM_WI_R_1), 1)
    );
    auto ls_1 = atf::cf::LS(
              (OCL_DIM_L_1 == 0) * (NUM_WI_L_1)
            + (OCL_DIM_L_2 == 0) * (NUM_WI_L_2)
            + (OCL_DIM_L_3 == 0) * (NUM_WI_L_3)
            + (OCL_DIM_L_4 == 0) * (NUM_WI_L_4)
            + (OCL_DIM_R_1 == 0) * (NUM_WI_R_1),

              (OCL_DIM_L_1 == 1) * (NUM_WI_L_1)
            + (OCL_DIM_L_2 == 1) * (NUM_WI_L_2)
            + (OCL_DIM_L_3 == 1) * (NUM_WI_L_3)
            + (OCL_DIM_L_4 == 1) * (NUM_WI_L_4)
            + (OCL_DIM_R_1 == 1) * (NUM_WI_R_1),

              atf::max((OCL_DIM_L_1 >= 2) * (NUM_WI_L_1), 1)
            * atf::max((OCL_DIM_L_2 >= 2) * (NUM_WI_L_2), 1)
            * atf::max((OCL_DIM_L_3 >= 2) * (NUM_WI_L_3), 1)
            * atf::max((OCL_DIM_L_4 >= 2) * (NUM_WI_L_4), 1)
            * atf::max((OCL_DIM_R_1 >= 2) * (NUM_WI_R_1), 1)
    );
    auto gs_2 = atf::cf::GS(
              (OCL_DIM_L_1 == 0) * (NUM_WG_L_1) * (NUM_WI_L_1)
            + (OCL_DIM_L_2 == 0) * (NUM_WG_L_2) * (NUM_WI_L_2)
            + (OCL_DIM_L_3 == 0) * (NUM_WG_L_3) * (NUM_WI_L_3)
            + (OCL_DIM_L_4 == 0) * (NUM_WG_L_4) * (NUM_WI_L_4)
            + (OCL_DIM_R_1 == 0) * (NUM_WI_R_1),

              (OCL_DIM_L_1 == 1) * (NUM_WG_L_1) * (NUM_WI_L_1)
            + (OCL_DIM_L_2 == 1) * (NUM_WG_L_2) * (NUM_WI_L_2)
            + (OCL_DIM_L_3 == 1) * (NUM_WG_L_3) * (NUM_WI_L_3)
            + (OCL_DIM_L_4 == 1) * (NUM_WG_L_4) * (NUM_WI_L_4)
            + (OCL_DIM_R_1 == 1) * (NUM_WI_R_1),

              atf::max((OCL_DIM_L_1 >= 2) * (NUM_WG_L_1) * (NUM_WI_L_1), 1)
            * atf::max((OCL_DIM_L_2 >= 2) * (NUM_WG_L_2) * (NUM_WI_L_2), 1)
            * atf::max((OCL_DIM_L_3 >= 2) * (NUM_WG_L_3) * (NUM_WI_L_3), 1)
            * atf::max((OCL_DIM_L_4 >= 2) * (NUM_WG_L_4) * (NUM_WI_L_4), 1)
            * atf::max((OCL_DIM_R_1 >= 2) * (NUM_WI_R_1), 1)
    );
    auto ls_2 = atf::cf::LS(
              (OCL_DIM_L_1 == 0) * (NUM_WI_L_1)
            + (OCL_DIM_L_2 == 0) * (NUM_WI_L_2)
            + (OCL_DIM_L_3 == 0) * (NUM_WI_L_3)
            + (OCL_DIM_L_4 == 0) * (NUM_WI_L_4)
            + (OCL_DIM_R_1 == 0) * (NUM_WI_R_1),

              (OCL_DIM_L_1 == 1) * (NUM_WI_L_1)
            + (OCL_DIM_L_2 == 1) * (NUM_WI_L_2)
            + (OCL_DIM_L_3 == 1) * (NUM_WI_L_3)
            + (OCL_DIM_L_4 == 1) * (NUM_WI_L_4)
            + (OCL_DIM_R_1 == 1) * (NUM_WI_R_1),

              atf::max((OCL_DIM_L_1 >= 2) * (NUM_WI_L_1), 1)
            * atf::max((OCL_DIM_L_2 >= 2) * (NUM_WI_L_2), 1)
            * atf::max((OCL_DIM_L_3 >= 2) * (NUM_WI_L_3), 1)
            * atf::max((OCL_DIM_L_4 >= 2) * (NUM_WI_L_4), 1)
            * atf::max((OCL_DIM_R_1 >= 2) * (NUM_WI_R_1), 1)
    );
    atf::cf::process_wrapper_info process_wrapper = {
            atf::cf::NONE,
            "none",
            "none",
            atf::cf::CHECK_NONE,
            "wrap"
    };
    atf::cf::timeout warm_up_timeout = {
            atf::cf::ABSOLUTE,
            {.absolute = 0}
    };
    atf::cf::timeout evaluation_timeout = {
            atf::cf::ABSOLUTE,
            {.absolute = 0}
    };
    auto kernel = atf::cf::ocl_md_hom(
            device_info,
            {atf::cf::kernel_info::FILENAME, "../kernel/multi_channel_convolution_1.cl", "multi_channel_convolution_1", " -DTYPE_T=float -DTYPE_TS=float"},
            atf::inputs(atf::buffer(in), atf::buffer(weights)),
            gs_1, ls_1,
            {atf::cf::kernel_info::FILENAME, "../kernel/multi_channel_convolution_2.cl", "multi_channel_convolution_2"},
            atf::inputs(atf::buffer(out)),
            gs_2, ls_2,
            res_g_size,
            atf::buffer(int_res),
            int_res_size,
            needs_second_kernel,
            is_valid,
            1, 0, // 0 warm ups and 1 evaluation because proper measurement is done below
            true,
            process_wrapper,
            warm_up_timeout,
            evaluation_timeout
    );

    // measure runtime
    std::cout << "measuring runtime... " << std::endl;
    std::cout.setstate(std::ios_base::failbit);
    auto best_config = tuner(kernel);
    std::cout.clear();
    kernel.warm_ups(10);
    kernel.evaluations(200);
    std::vector<std::vector<unsigned long long>> runtimes;
    kernel(best_config, &runtimes);

    // print runtime
    unsigned long long runtime = 0;
    for (const auto &times : runtimes) {
        if (!times.empty())
            runtime += *std::min_element(times.begin(), times.end());
    }
    std::cout << "kernel took " << (runtime / 1000000.0f) << "ms" << std::endl;
}

int main(int argc, const char **argv) {
    size_t platform_id = std::atoi(argv[1]);
    size_t device_id   = std::atoi(argv[2]);
    const size_t N = 1, K = 512, P = 5, Q = 5, C = 512;

    // get device
    atf::cf::device_info device_info(static_cast<const int &>(platform_id), static_cast<const int &>(device_id));
    device_info.initialize();

    // prepare inputs
    std::vector<float> in(N * C * (P + 2) * (Q + 2)); for (int i = 0; i < in.size(); ++i) in[i] = (i % 100) + 1;
    std::vector<struct WEIGHTS_33> weights(K * C * 3 * 3);
    int i = 1;
    for (int k = 0; k < K; ++k) {
        for (int c = 0; c < C; ++c) {
            for (int r = 0; r < 3; ++r) {
                for (int s = 0; s < 3; ++s) {
                    weights[k * C + c].values[r][s] = i++;
                    if (i == 100) i = 1;
                }
            }
        }
    }
    std::vector<float> out(N * K * P * Q); for (int i = 0; i < out.size(); ++i) out[i] = 0;

    // benchmark
    bench(platform_id, device_id, device_info, N, K, P, Q, C, in, weights, out);
}