#!/usr/bin/env bash

(cd build && cmake .. && make) && \

echo ""
echo ""
echo ""
echo ""
echo ""
echo ""
echo ""


OCL_PLATFORM=0
OCL_DEVICE=0
echo "" && \
echo "" && \
echo "Running md_poly on Intel Xeon E5 2640 v2 CPU" && \
echo "" && \
echo "matrix multiplication with rw input size " && \
(cd build && ./gemm_intel_xeon_e5_2640_v2_rw $OCL_PLATFORM $OCL_DEVICE) && \
echo "" && \
echo "matrix multiplication with pp input size " && \
(cd build && ./gemm_intel_xeon_e5_2640_v2_pp $OCL_PLATFORM $OCL_DEVICE) && \
echo "" && \
echo "gaussian convolution with rw input size " && \
(cd build && ./conv2d_intel_xeon_e5_2640_v2_rw $OCL_PLATFORM $OCL_DEVICE) && \
echo "" && \
echo "gaussian convolution with pp input size " && \
(cd build && ./conv2d_intel_xeon_e5_2640_v2_pp $OCL_PLATFORM $OCL_DEVICE) && \

echo "" && \
echo "" && \
echo "Running PPCG on Intel Xeon E5 2640 v2 CPU" && \
echo "" && \
echo "matrix multiplication with rw input size " && \
(cd ppcg/intel_xeon_e5_2640_v2/gemm/rw && source run.sh $OCL_PLATFORM $OCL_DEVICE) && \
echo "" && \
echo "matrix multiplication with pp input size " && \
(cd ppcg/intel_xeon_e5_2640_v2/gemm/pp && source run.sh $OCL_PLATFORM $OCL_DEVICE) && \
echo "" && \
echo "gaussian convolution with rw input size " && \
(cd ppcg/intel_xeon_e5_2640_v2/conv2d/rw && source run.sh $OCL_PLATFORM $OCL_DEVICE) && \
echo "" && \
echo "gaussian convolution with pp input size " && \
(cd ppcg/intel_xeon_e5_2640_v2/conv2d/pp && source run.sh $OCL_PLATFORM $OCL_DEVICE) && \




OCL_PLATFORM=1
OCL_DEVICE=0
echo "" && \
echo "" && \
echo "Running md_poly on Tesla V100-SXM2-16GB" && \
echo "" && \
echo "matrix multiplication with rw input size " && \
(cd build && ./gemm_tesla_v100_sxm2_16gb_rw $OCL_PLATFORM $OCL_DEVICE) && \
echo "" && \
echo "matrix multiplication with pp input size " && \
(cd build && ./gemm_tesla_v100_sxm2_16gb_pp $OCL_PLATFORM $OCL_DEVICE) && \
echo "" && \
echo "gaussian convolution with rw input size " && \
(cd build && ./conv2d_tesla_v100_sxm2_16gb_rw $OCL_PLATFORM $OCL_DEVICE) && \
echo "" && \
echo "gaussian convolution with pp input size " && \
(cd build && ./conv2d_tesla_v100_sxm2_16gb_pp $OCL_PLATFORM $OCL_DEVICE) && \

echo "" && \
echo "" && \
echo "Running PPCG on Tesla V100-SXM2-16GB" && \
echo "" && \
echo "matrix multiplication with rw input size " && \
(cd ppcg/tesla_v100_sxm2_16gb/gemm/rw && source run.sh $OCL_PLATFORM $OCL_DEVICE) && \
echo "" && \
echo "matrix multiplication with pp input size " && \
(cd ppcg/tesla_v100_sxm2_16gb/gemm/pp && source run.sh $OCL_PLATFORM $OCL_DEVICE) && \
echo "" && \
echo "gaussian convolution with rw input size " && \
(cd ppcg/tesla_v100_sxm2_16gb/conv2d/rw && source run.sh $OCL_PLATFORM $OCL_DEVICE) && \
echo "" && \
echo "gaussian convolution with pp input size " && \
(cd ppcg/tesla_v100_sxm2_16gb/conv2d/pp && source run.sh $OCL_PLATFORM $OCL_DEVICE)