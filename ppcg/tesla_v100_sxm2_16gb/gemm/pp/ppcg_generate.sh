#!/usr/bin/env bash

c_file="sgemm_row_major_nn.c"
host_file="sgemm_row_major_nn_host"
kernel_file="sgemm_row_major_nn_kernel"
host_file="$host_file.cu"
kernel_file="$kernel_file.cu"
dimensionality=3
warm_ups=10
evaluations=200

NUM_WG_0=76
NUM_WG_1=2
NUM_WG_2=12
NUM_WI_0=1
NUM_WI_1=512
NUM_WI_2=64
TILE_SIZE_0=14
TILE_SIZE_1=512
TILE_SIZE_2=84

# run ppcg code generator
sizes="{ kernel[i0] -> grid["
for (( i=0; i<$((dimensionality>3 ? 3 : dimensionality)); i++ )); do
    [[ $i > 0 ]] && sizes="${sizes},"
    sizes="${sizes}o$i"
done
sizes="${sizes}] : i0 = 0"
for (( i=0; i<$((dimensionality>3 ? 3 : dimensionality)); i++ )); do
    NUM_WG="NUM_WG_$i"
    sizes="${sizes} and o$i = ${!NUM_WG}"
done
sizes="${sizes}; kernel[i0] -> block["
for (( i=0; i<$((dimensionality>3 ? 3 : dimensionality)); i++ )); do
    [[ $i > 0 ]] && sizes="${sizes},"
    sizes="${sizes}o$i"
done
sizes="${sizes}] : i0 = 0"
for (( i=0; i<$((dimensionality>3 ? 3 : dimensionality)); i++ )); do
    NUM_WI="NUM_WI_$i"
    sizes="${sizes} and o$i = ${!NUM_WI}"
done
sizes="${sizes}; kernel[i0] -> tile["
for (( i=0; i<dimensionality; i++ )); do
    [[ $i > 0 ]] && sizes="${sizes},"
    sizes="${sizes}o$i"
done
sizes="${sizes}] : i0 = 0"
for (( i=0; i<$dimensionality; i++ )); do
    TILE_SIZE="TILE_SIZE_$i"
    sizes="${sizes} and o$i = ${!TILE_SIZE}"
done
sizes="${sizes} }"

ppcg --target=cuda --sizes="$sizes" $c_file
SUCCESS=$?
if [ $SUCCESS -ne 0 ];
then
    echo "ppcg failed with status $SUCCESS"
    exit $SUCCESS
fi

# add warm ups, evaluations, profiling, and result check to generated host code
sed -i "s/#include <assert.h>/#include <assert.h>\n#include <limits.h>\n#include <math.h>/g" $host_file && \
write_buffer_calls=`grep --color=never -oE "cudaCheckReturn\(cudaMemcpy\(.*cudaMemcpyHostToDevice\)\);$" $host_file` && \
enqueue_kernel_call=`grep --color=never -oE "kernel0 <<<.*;$" $host_file` && \
enqueue_kernel_call_with_timing="${write_buffer_calls}
    cudaEvent_t start, stop;
    cudaEventCreate(\\\\&start);
    cudaEventCreate(\\\\&stop);
    cudaEventRecord(start);
    ${enqueue_kernel_call}
    cudaCheckKernel();
    cudaEventRecord(stop);
    cudaEventSynchronize(stop);
    float milliseconds = 0;
    cudaEventElapsedTime(\\\\&milliseconds, start, stop);
    unsigned long runtime = milliseconds * 1000000;
    if (runtime < min_runtime) min_runtime = runtime;" && \
warm_ups_and_evaluations="unsigned long min_runtime = ULONG_MAX;
for (int i = 0; i < $warm_ups; ++i) {
    $enqueue_kernel_call_with_timing
}
min_runtime = ULONG_MAX;
for (int i = 0; i < $evaluations; ++i) {
    $enqueue_kernel_call_with_timing
}
printf(\"%lu\", min_runtime);" && \
sed "s/${enqueue_kernel_call}/enqueueCall/g" $host_file > tmp && \
awk -v new="${warm_ups_and_evaluations}" '{sub(/enqueueCall/, new);print;}' tmp > $host_file && \
rm tmp
