#include "sgemm_row_major_nn_kernel.hu"
__global__ void kernel0(float *A, float *B, float *C)
{
    int b0 = blockIdx.y, b1 = blockIdx.x;
    int t0 = threadIdx.y, t1 = threadIdx.x;
    __shared__ float shared_A[14][84];
    float private_C[14][1];

    #define ppcg_min(x,y)    ({ __typeof__(x) _x = (x); __typeof__(y) _y = (y); _x < _y ? _x : _y; })
    {
      private_C[0][0] = C[14 * b0 * 1024 + (512 * b1 + t1)];
      private_C[1][0] = C[(14 * b0 + 1) * 1024 + (512 * b1 + t1)];
      if (b0 <= 72) {
        private_C[2][0] = C[(14 * b0 + 2) * 1024 + (512 * b1 + t1)];
        private_C[3][0] = C[(14 * b0 + 3) * 1024 + (512 * b1 + t1)];
        private_C[4][0] = C[(14 * b0 + 4) * 1024 + (512 * b1 + t1)];
        private_C[5][0] = C[(14 * b0 + 5) * 1024 + (512 * b1 + t1)];
        private_C[6][0] = C[(14 * b0 + 6) * 1024 + (512 * b1 + t1)];
        private_C[7][0] = C[(14 * b0 + 7) * 1024 + (512 * b1 + t1)];
        private_C[8][0] = C[(14 * b0 + 8) * 1024 + (512 * b1 + t1)];
        private_C[9][0] = C[(14 * b0 + 9) * 1024 + (512 * b1 + t1)];
        private_C[10][0] = C[(14 * b0 + 10) * 1024 + (512 * b1 + t1)];
        private_C[11][0] = C[(14 * b0 + 11) * 1024 + (512 * b1 + t1)];
        private_C[12][0] = C[(14 * b0 + 12) * 1024 + (512 * b1 + t1)];
        private_C[13][0] = C[(14 * b0 + 13) * 1024 + (512 * b1 + t1)];
      }
      for (int c2 = 0; c2 <= 1023; c2 += 84) {
        if (t1 <= 83 && t1 + c2 <= 1023)
          for (int c3 = 0; c3 <= ppcg_min(13, -14 * b0 + 1023); c3 += 1)
            shared_A[c3][t1] = A[(14 * b0 + c3) * 1024 + (t1 + c2)];
        __syncthreads();
        for (int c3 = 0; c3 <= ppcg_min(83, -c2 + 1023); c3 += 1) {
          private_C[0][0] += (shared_A[0][c3] * B[(c2 + c3) * 1024 + (512 * b1 + t1)]);
          private_C[1][0] += (shared_A[1][c3] * B[(c2 + c3) * 1024 + (512 * b1 + t1)]);
          if (b0 <= 72) {
            private_C[2][0] += (shared_A[2][c3] * B[(c2 + c3) * 1024 + (512 * b1 + t1)]);
            private_C[3][0] += (shared_A[3][c3] * B[(c2 + c3) * 1024 + (512 * b1 + t1)]);
            private_C[4][0] += (shared_A[4][c3] * B[(c2 + c3) * 1024 + (512 * b1 + t1)]);
            private_C[5][0] += (shared_A[5][c3] * B[(c2 + c3) * 1024 + (512 * b1 + t1)]);
            private_C[6][0] += (shared_A[6][c3] * B[(c2 + c3) * 1024 + (512 * b1 + t1)]);
            private_C[7][0] += (shared_A[7][c3] * B[(c2 + c3) * 1024 + (512 * b1 + t1)]);
            private_C[8][0] += (shared_A[8][c3] * B[(c2 + c3) * 1024 + (512 * b1 + t1)]);
            private_C[9][0] += (shared_A[9][c3] * B[(c2 + c3) * 1024 + (512 * b1 + t1)]);
            private_C[10][0] += (shared_A[10][c3] * B[(c2 + c3) * 1024 + (512 * b1 + t1)]);
            private_C[11][0] += (shared_A[11][c3] * B[(c2 + c3) * 1024 + (512 * b1 + t1)]);
            private_C[12][0] += (shared_A[12][c3] * B[(c2 + c3) * 1024 + (512 * b1 + t1)]);
            private_C[13][0] += (shared_A[13][c3] * B[(c2 + c3) * 1024 + (512 * b1 + t1)]);
          }
        }
        __syncthreads();
      }
      C[14 * b0 * 1024 + (512 * b1 + t1)] = private_C[0][0];
      C[(14 * b0 + 1) * 1024 + (512 * b1 + t1)] = private_C[1][0];
      if (b0 <= 72) {
        C[(14 * b0 + 2) * 1024 + (512 * b1 + t1)] = private_C[2][0];
        C[(14 * b0 + 3) * 1024 + (512 * b1 + t1)] = private_C[3][0];
        C[(14 * b0 + 4) * 1024 + (512 * b1 + t1)] = private_C[4][0];
        C[(14 * b0 + 5) * 1024 + (512 * b1 + t1)] = private_C[5][0];
        C[(14 * b0 + 6) * 1024 + (512 * b1 + t1)] = private_C[6][0];
        C[(14 * b0 + 7) * 1024 + (512 * b1 + t1)] = private_C[7][0];
        C[(14 * b0 + 8) * 1024 + (512 * b1 + t1)] = private_C[8][0];
        C[(14 * b0 + 9) * 1024 + (512 * b1 + t1)] = private_C[9][0];
        C[(14 * b0 + 10) * 1024 + (512 * b1 + t1)] = private_C[10][0];
        C[(14 * b0 + 11) * 1024 + (512 * b1 + t1)] = private_C[11][0];
        C[(14 * b0 + 12) * 1024 + (512 * b1 + t1)] = private_C[12][0];
        C[(14 * b0 + 13) * 1024 + (512 * b1 + t1)] = private_C[13][0];
      }
    }
}
