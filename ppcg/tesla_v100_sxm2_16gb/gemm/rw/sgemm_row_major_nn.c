int main() {
    #define M_VAL 10
    #define N_VAL 500
    #define K_VAL 64

    static float A[M_VAL][K_VAL];
    static float B[K_VAL][N_VAL];
    static float C[M_VAL][N_VAL];

    for (int i = 0; i < M_VAL * K_VAL; ++i) ((float *)A)[i] = (i % 10) + 1;
    for (int i = 0; i < K_VAL * N_VAL; ++i) ((float *)B)[i] = (i % 10) + 1;
    for (int i = 0; i < M_VAL * N_VAL; ++i) ((float *)C)[i] = 0;

#pragma scop
    for (int i = 0; i < M_VAL; ++i)
        for (int j = 0; j < N_VAL; ++j)
            for (int k = 0; k < K_VAL; ++k)
                C[i][j] += A[i][k] * B[k][j];
#pragma endscop

#ifdef WRITE_GOLD
    WRITE_GOLD
#endif
}