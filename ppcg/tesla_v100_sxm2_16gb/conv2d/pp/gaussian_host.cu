#include <assert.h>
#include <limits.h>
#include <math.h>
#include <stdio.h>
#include "gaussian_kernel.hu"
int main() {
    #define M_VAL 4092
    #define N_VAL 4092

    static float in[M_VAL + 4][N_VAL + 4];
    static float out[M_VAL][N_VAL];

    for (int i = 0; i < (M_VAL + 4) * (N_VAL + 4); ++i) ((float *)in)[i] = (i % 10) + 1;
    for (int i = 0; i < M_VAL * N_VAL; ++i) ((float *)out)[i] = 0;

    {
#define cudaCheckReturn(ret) \
  do { \
    cudaError_t cudaCheckReturn_e = (ret); \
    if (cudaCheckReturn_e != cudaSuccess) { \
      fprintf(stderr, "CUDA error: %s\n", cudaGetErrorString(cudaCheckReturn_e)); \
      fflush(stderr); \
    } \
    assert(cudaCheckReturn_e == cudaSuccess); \
  } while(0)
#define cudaCheckKernel() \
  do { \
    cudaCheckReturn(cudaGetLastError()); \
  } while(0)

      float *dev_in;
      float *dev_out;
      
      cudaCheckReturn(cudaMalloc((void **) &dev_in, (4096) * (4096) * sizeof(float)));
      cudaCheckReturn(cudaMalloc((void **) &dev_out, (4092) * (4092) * sizeof(float)));
      
      cudaCheckReturn(cudaMemcpy(dev_in, in, (4096) * (4096) * sizeof(float), cudaMemcpyHostToDevice));
      {
        dim3 k0_dimBlock(256, 1);
        dim3 k0_dimGrid(16, 1023);
        unsigned long min_runtime = ULONG_MAX;
for (int i = 0; i < 10; ++i) {
    cudaCheckReturn(cudaMemcpy(dev_in, in, (4096) * (4096) * sizeof(float), cudaMemcpyHostToDevice));
    cudaEvent_t start, stop;
    cudaEventCreate(&start);
    cudaEventCreate(&stop);
    cudaEventRecord(start);
    kernel0 <<<k0_dimGrid, k0_dimBlock>>> (dev_in, dev_out);
    cudaCheckKernel();
    cudaEventRecord(stop);
    cudaEventSynchronize(stop);
    float milliseconds = 0;
    cudaEventElapsedTime(&milliseconds, start, stop);
    unsigned long runtime = milliseconds * 1000000;
    if (runtime < min_runtime) min_runtime = runtime;
}
min_runtime = ULONG_MAX;
for (int i = 0; i < 200; ++i) {
    cudaCheckReturn(cudaMemcpy(dev_in, in, (4096) * (4096) * sizeof(float), cudaMemcpyHostToDevice));
    cudaEvent_t start, stop;
    cudaEventCreate(&start);
    cudaEventCreate(&stop);
    cudaEventRecord(start);
    kernel0 <<<k0_dimGrid, k0_dimBlock>>> (dev_in, dev_out);
    cudaCheckKernel();
    cudaEventRecord(stop);
    cudaEventSynchronize(stop);
    float milliseconds = 0;
    cudaEventElapsedTime(&milliseconds, start, stop);
    unsigned long runtime = milliseconds * 1000000;
    if (runtime < min_runtime) min_runtime = runtime;
}
printf("%lu", min_runtime);
        cudaCheckKernel();
      }
      
      cudaCheckReturn(cudaMemcpy(out, dev_out, (4092) * (4092) * sizeof(float), cudaMemcpyDeviceToHost));
      cudaCheckReturn(cudaFree(dev_in));
      cudaCheckReturn(cudaFree(dev_out));
    }

#ifdef WRITE_GOLD
    WRITE_GOLD
#endif
}
