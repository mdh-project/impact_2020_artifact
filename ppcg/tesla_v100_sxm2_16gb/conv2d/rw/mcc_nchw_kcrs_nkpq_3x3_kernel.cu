#include "mcc_nchw_kcrs_nkpq_3x3_kernel.hu"
__global__ void kernel0(WEIGHTS *filter, float *in, float *out)
{
    int b0 = blockIdx.y, b1 = blockIdx.x;
    int t0 = threadIdx.z, t1 = threadIdx.y, t2 = threadIdx.x;
    __shared__ float shared_in[1][1][6][7];
    float private_out[1][4][1][1];

    #define ppcg_min(x,y)    ({ __typeof__(x) _x = (x); __typeof__(y) _y = (y); _x < _y ? _x : _y; })
    {
      if (4 * b1 + t1 <= 4) {
        private_out[0][0][0][0] = out[((0 * 512 + (64 * b0 + t0)) * 5 + (4 * b1 + t1)) * 5 + t2];
        private_out[0][1][0][0] = out[((0 * 512 + (64 * b0 + t0 + 16)) * 5 + (4 * b1 + t1)) * 5 + t2];
        private_out[0][2][0][0] = out[((0 * 512 + (64 * b0 + t0 + 32)) * 5 + (4 * b1 + t1)) * 5 + t2];
        private_out[0][3][0][0] = out[((0 * 512 + (64 * b0 + t0 + 48)) * 5 + (4 * b1 + t1)) * 5 + t2];
      }
      for (int c3 = 0; c3 <= 511; c3 += 1) {
        if (t0 == 0)
          for (int c6 = t1; c6 <= ppcg_min(5, -4 * b1 + 6); c6 += 4)
            for (int c7 = t2; c7 <= 6; c7 += 5)
              shared_in[0][0][c6][c7] = in[((0 * 512 + c3) * 7 + (4 * b1 + c6)) * 7 + c7];
        __syncthreads();
        if (4 * b1 + t1 <= 4)
          for (int c4 = t0; c4 <= 63; c4 += 16)
            for (int c8 = -1; c8 <= 1; c8 += 1)
              for (int c9 = -1; c9 <= 1; c9 += 1)
                private_out[0][(-t0 + c4) / 16][0][0] += (filter[(64 * b0 + c4) * 512 + c3].values[c8 + 1][c9 + 1] * shared_in[0][0][t1 + c8 + 1][t2 + c9 + 1]);
        __syncthreads();
      }
      if (4 * b1 + t1 <= 4) {
        out[((0 * 512 + (64 * b0 + t0)) * 5 + (4 * b1 + t1)) * 5 + t2] = private_out[0][0][0][0];
        out[((0 * 512 + (64 * b0 + t0 + 16)) * 5 + (4 * b1 + t1)) * 5 + t2] = private_out[0][1][0][0];
        out[((0 * 512 + (64 * b0 + t0 + 32)) * 5 + (4 * b1 + t1)) * 5 + t2] = private_out[0][2][0][0];
        out[((0 * 512 + (64 * b0 + t0 + 48)) * 5 + (4 * b1 + t1)) * 5 + t2] = private_out[0][3][0][0];
      }
    }
}
