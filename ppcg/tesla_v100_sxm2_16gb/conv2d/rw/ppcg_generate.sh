#!/usr/bin/env bash

c_file="mcc_nchw_kcrs_nkpq_3x3.c"
host_file="mcc_nchw_kcrs_nkpq_3x3_host"
kernel_file="mcc_nchw_kcrs_nkpq_3x3_kernel"
host_file="$host_file.cu"
kernel_file="$kernel_file.cu"
dimensionality=5
warm_ups=10
evaluations=200

NUM_WG_0=16
NUM_WG_1=32
NUM_WG_2=4
NUM_WI_0=16
NUM_WI_1=4
NUM_WI_2=8
TILE_SIZE_0=64
TILE_SIZE_1=4
TILE_SIZE_2=128
TILE_SIZE_3=1
TILE_SIZE_4=1

# run ppcg code generator
sizes="{ kernel[i0] -> grid["
for (( i=0; i<$((dimensionality>3 ? 3 : dimensionality)); i++ )); do
    [[ $i > 0 ]] && sizes="${sizes},"
    sizes="${sizes}o$i"
done
sizes="${sizes}] : i0 = 0"
for (( i=0; i<$((dimensionality>3 ? 3 : dimensionality)); i++ )); do
    NUM_WG="NUM_WG_$i"
    sizes="${sizes} and o$i = ${!NUM_WG}"
done
sizes="${sizes}; kernel[i0] -> block["
for (( i=0; i<$((dimensionality>3 ? 3 : dimensionality)); i++ )); do
    [[ $i > 0 ]] && sizes="${sizes},"
    sizes="${sizes}o$i"
done
sizes="${sizes}] : i0 = 0"
for (( i=0; i<$((dimensionality>3 ? 3 : dimensionality)); i++ )); do
    NUM_WI="NUM_WI_$i"
    sizes="${sizes} and o$i = ${!NUM_WI}"
done
sizes="${sizes}; kernel[i0] -> tile["
for (( i=0; i<dimensionality; i++ )); do
    [[ $i > 0 ]] && sizes="${sizes},"
    sizes="${sizes}o$i"
done
sizes="${sizes}] : i0 = 0"
for (( i=0; i<$dimensionality; i++ )); do
    TILE_SIZE="TILE_SIZE_$i"
    sizes="${sizes} and o$i = ${!TILE_SIZE}"
done
sizes="${sizes} }"

ppcg --target=cuda --sizes="$sizes" $c_file
SUCCESS=$?
if [ $SUCCESS -ne 0 ];
then
    echo "ppcg failed with status $SUCCESS"
    exit $SUCCESS
fi

# add warm ups, evaluations, profiling, and result check to generated host code
sed -i "s/#include <assert.h>/#include <assert.h>\n#include <limits.h>\n#include <math.h>/g" $host_file && \
write_buffer_calls=`grep --color=never -oE "cudaCheckReturn\(cudaMemcpy\(.*cudaMemcpyHostToDevice\)\);$" $host_file` && \
enqueue_kernel_call=`grep --color=never -oE "kernel0 <<<.*;$" $host_file` && \
enqueue_kernel_call_with_timing="${write_buffer_calls}
    cudaEvent_t start, stop;
    cudaEventCreate(\\\\&start);
    cudaEventCreate(\\\\&stop);
    cudaEventRecord(start);
    ${enqueue_kernel_call}
    cudaCheckKernel();
    cudaEventRecord(stop);
    cudaEventSynchronize(stop);
    float milliseconds = 0;
    cudaEventElapsedTime(\\\\&milliseconds, start, stop);
    unsigned long runtime = milliseconds * 1000000;
    if (runtime < min_runtime) min_runtime = runtime;" && \
warm_ups_and_evaluations="unsigned long min_runtime = ULONG_MAX;
for (int i = 0; i < $warm_ups; ++i) {
    $enqueue_kernel_call_with_timing
}
min_runtime = ULONG_MAX;
for (int i = 0; i < $evaluations; ++i) {
    $enqueue_kernel_call_with_timing
}
printf(\"%lu\", min_runtime);" && \
sed "s/${enqueue_kernel_call}/enqueueCall/g" $host_file > tmp && \
awk -v new="${warm_ups_and_evaluations}" '{sub(/enqueueCall/, new);print;}' tmp > $host_file && \
rm tmp && \

# fix definition of structs
struct_defs=`grep -ozP --color=never "#include \"mcc_nchw_kcrs_nkpq_3x3_kernel.hu\"\n\K(.|\n)*(?=\n__global__)" $kernel_file`
if [[ ! -z "$struct_defs" ]]; then
    struct_defs_start=`grep -n "#include \"mcc_nchw_kcrs_nkpq_3x3_kernel.hu\"" $host_file | cut -f1 -d:` && \
    struct_defs_end=`grep -n "int main" $host_file | cut -f1 -d:` && \
    sed -i "$((struct_defs_start+1)),$((struct_defs_end-1))d" $host_file && \
    struct_defs_start=`grep -n "#include \"mcc_nchw_kcrs_nkpq_3x3_kernel.hu\"" $kernel_file | cut -f1 -d:` && \
    struct_defs_end=`grep -n "__global__ void" $kernel_file | cut -f1 -d:` && \
    sed -i "$((struct_defs_start+1)),$((struct_defs_end-1))d" $kernel_file && \
    echo "$struct_defs" > tmp && \
    struct_defs_start=`grep -n "#include \"cuda.h\"" mcc_nchw_kcrs_nkpq_3x3_kernel.hu | cut -f1 -d:` && \
    sed -i "$((struct_defs_start+1))r tmp" mcc_nchw_kcrs_nkpq_3x3_kernel.hu
fi