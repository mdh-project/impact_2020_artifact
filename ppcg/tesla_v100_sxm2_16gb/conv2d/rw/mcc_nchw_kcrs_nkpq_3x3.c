typedef struct {
    float values[3][3];
} WEIGHTS;

int main() {
    #define N_VAL 1
    #define K_VAL 512
    #define P_VAL 5
    #define Q_VAL 5
    #define C_VAL 512

    static float in[N_VAL][C_VAL][P_VAL + 2][Q_VAL + 2];
    static WEIGHTS filter[K_VAL][C_VAL];
    static float out[N_VAL][K_VAL][P_VAL][Q_VAL];

    for (int i = 0; i < N_VAL * C_VAL * (P_VAL + 2) * (Q_VAL + 2); ++i) ((float *)in)[i] = (i % 10) + 1;
    for (int i = 0; i < K_VAL * C_VAL * 3 * 3; ++i) ((float *)filter)[i] = (i % 10) + 1;
    for (int i = 0; i < N_VAL * K_VAL * P_VAL * Q_VAL; ++i) ((float *)out)[i] = 0;

#pragma scop
    for (int n = 0; n < N_VAL; n++) {
        for (int k = 0; k < K_VAL; k++) {
            for (int p = 0; p < P_VAL; p++) {
                for (int q = 0; q < Q_VAL; q++) {
                    for (int c = 0; c < C_VAL; c++) {
                        for (int r = -1; r <= 1; ++r){
                            for (int s = -1; s <= 1; ++s){
                                out[n][k][p][q] += filter[k][c].values[1 + r][1 + s] * in[n][c][1 + p + r][1 + q + s];
                            }
                        }
                    }
                }
            }
        }
    }
#pragma endscop

#ifdef WRITE_GOLD
    WRITE_GOLD
#endif
}