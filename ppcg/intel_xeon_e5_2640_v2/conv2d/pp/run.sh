#!/usr/bin/env bash

gcc -std=c99 gaussian_host.c ocl_utilities.c -lOpenCL && \

echo "measuring runtime..." && \
./a.out $1 $2 && \
echo "ns"