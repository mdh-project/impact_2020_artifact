#include <assert.h>
#include <limits.h>
#include <math.h>
#include <stdio.h>
#include "ocl_utilities.h"
typedef struct {
    float values[3][3];
} WEIGHTS;

int main(int argc, const char **argv) {
int platform_id = strtol(argv[1], NULL, 10);
int device_id = strtol(argv[2], NULL, 10);

    #define N_VAL 1
    #define K_VAL 512
    #define P_VAL 5
    #define Q_VAL 5
    #define C_VAL 512

    static float in[N_VAL][C_VAL][P_VAL + 2][Q_VAL + 2];
    static WEIGHTS filter[K_VAL][C_VAL];
    static float out[N_VAL][K_VAL][P_VAL][Q_VAL];

    for (int i = 0; i < N_VAL * C_VAL * (P_VAL + 2) * (Q_VAL + 2); ++i) ((float *)in)[i] = (i % 10) + 1;
    for (int i = 0; i < K_VAL * C_VAL * 3 * 3; ++i) ((float *)filter)[i] = (i % 10) + 1;
    for (int i = 0; i < N_VAL * K_VAL * P_VAL * Q_VAL; ++i) ((float *)out)[i] = 0;

    {
      #define openclCheckReturn(ret) \
  if (ret != CL_SUCCESS) {\
    fprintf(stderr, "OpenCL error: %s\n", opencl_error_string(ret)); \
    fflush(stderr); \
    assert(ret == CL_SUCCESS);\
  }

      cl_mem dev_filter;
      cl_mem dev_in;
      cl_mem dev_out;
      
      cl_device_id device;
      cl_context context;
      cl_program program;
      cl_command_queue queue;
      cl_int err;
      device = opencl_create_device(platform_id, device_id);
      context = clCreateContext(NULL, 1, &device, NULL, NULL, &err);
      openclCheckReturn(err);
      queue = clCreateCommandQueue(context, device, CL_QUEUE_PROFILING_ENABLE, &err);
      openclCheckReturn(err);
      program = opencl_build_program_from_file(context, device, "mcc_nchw_kcrs_nkpq_3x3_kernel.cl", "");
      
      {
        dev_filter = clCreateBuffer(context, CL_MEM_READ_WRITE, (512) * (512) * sizeof(WEIGHTS), NULL, &err);
        openclCheckReturn(err);
      }
      {
        dev_in = clCreateBuffer(context, CL_MEM_READ_WRITE, (1) * (512) * (7) * (7) * sizeof(float), NULL, &err);
        openclCheckReturn(err);
      }
      {
        dev_out = clCreateBuffer(context, CL_MEM_READ_WRITE, (1) * (512) * (5) * (5) * sizeof(float), NULL, &err);
        openclCheckReturn(err);
      }
      
      openclCheckReturn(clEnqueueWriteBuffer(queue, dev_filter, CL_TRUE, 0, (512) * (512) * sizeof(WEIGHTS), filter, 0, NULL, NULL));
      openclCheckReturn(clEnqueueWriteBuffer(queue, dev_in, CL_TRUE, 0, (1) * (512) * (7) * (7) * sizeof(float), in, 0, NULL, NULL));
      openclCheckReturn(clEnqueueWriteBuffer(queue, dev_out, CL_TRUE, 0, (1) * (512) * (5) * (5) * sizeof(float), out, 0, NULL, NULL));
      {
        size_t global_work_size[3] = {(1) * 1, (5) * 1, 1};
        size_t block_size[3] = {1, 1, 1};
        cl_kernel kernel0 = clCreateKernel(program, "kernel0", &err);
        openclCheckReturn(err);
        openclCheckReturn(clSetKernelArg(kernel0, 0, sizeof(cl_mem), (void *) &dev_filter));
        openclCheckReturn(clSetKernelArg(kernel0, 1, sizeof(cl_mem), (void *) &dev_in));
        openclCheckReturn(clSetKernelArg(kernel0, 2, sizeof(cl_mem), (void *) &dev_out));
        unsigned long min_runtime = ULONG_MAX;
for (int i = 0; i < 10; ++i) {
    openclCheckReturn(clEnqueueWriteBuffer(queue, dev_filter, CL_TRUE, 0, (512) * (512) * sizeof(WEIGHTS), filter, 0, NULL, NULL));
openclCheckReturn(clEnqueueWriteBuffer(queue, dev_in, CL_TRUE, 0, (1) * (512) * (7) * (7) * sizeof(float), in, 0, NULL, NULL));
openclCheckReturn(clEnqueueWriteBuffer(queue, dev_out, CL_TRUE, 0, (1) * (512) * (5) * (5) * sizeof(float), out, 0, NULL, NULL));
    cl_event event;
    openclCheckReturn(clEnqueueNDRangeKernel(queue, kernel0, 3, NULL, global_work_size, block_size, 0, NULL, &event));
    openclCheckReturn(clFinish(queue));
    cl_ulong start, end;
    openclCheckReturn(clGetEventProfilingInfo(event, CL_PROFILING_COMMAND_START, sizeof(cl_ulong), &start, NULL));
    openclCheckReturn(clGetEventProfilingInfo(event, CL_PROFILING_COMMAND_END, sizeof(cl_ulong), &end, NULL));
    unsigned long runtime = end - start;
    if (runtime < min_runtime) min_runtime = runtime;
}
min_runtime = ULONG_MAX;
for (int i = 0; i < 200; ++i) {
    openclCheckReturn(clEnqueueWriteBuffer(queue, dev_filter, CL_TRUE, 0, (512) * (512) * sizeof(WEIGHTS), filter, 0, NULL, NULL));
openclCheckReturn(clEnqueueWriteBuffer(queue, dev_in, CL_TRUE, 0, (1) * (512) * (7) * (7) * sizeof(float), in, 0, NULL, NULL));
openclCheckReturn(clEnqueueWriteBuffer(queue, dev_out, CL_TRUE, 0, (1) * (512) * (5) * (5) * sizeof(float), out, 0, NULL, NULL));
    cl_event event;
    openclCheckReturn(clEnqueueNDRangeKernel(queue, kernel0, 3, NULL, global_work_size, block_size, 0, NULL, &event));
    openclCheckReturn(clFinish(queue));
    cl_ulong start, end;
    openclCheckReturn(clGetEventProfilingInfo(event, CL_PROFILING_COMMAND_START, sizeof(cl_ulong), &start, NULL));
    openclCheckReturn(clGetEventProfilingInfo(event, CL_PROFILING_COMMAND_END, sizeof(cl_ulong), &end, NULL));
    unsigned long runtime = end - start;
    if (runtime < min_runtime) min_runtime = runtime;
}
printf("%lu", min_runtime);
        openclCheckReturn(clReleaseKernel(kernel0));
        clFinish(queue);
      }
      
      openclCheckReturn(clEnqueueReadBuffer(queue, dev_out, CL_TRUE, 0, (1) * (512) * (5) * (5) * sizeof(float), out, 0, NULL, NULL));
      openclCheckReturn(clReleaseMemObject(dev_filter));
      openclCheckReturn(clReleaseMemObject(dev_in));
      openclCheckReturn(clReleaseMemObject(dev_out));
      openclCheckReturn(clReleaseCommandQueue(queue));
      openclCheckReturn(clReleaseProgram(program));
      openclCheckReturn(clReleaseContext(context));
    }

#ifdef WRITE_GOLD
    WRITE_GOLD
#endif
}
