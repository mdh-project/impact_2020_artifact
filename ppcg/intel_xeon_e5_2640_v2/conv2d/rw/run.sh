#!/usr/bin/env bash

gcc -std=c99 mcc_nchw_kcrs_nkpq_3x3_host.c ocl_utilities.c -lOpenCL && \

echo "measuring runtime..." && \
./a.out $1 $2 && \
echo "ns"