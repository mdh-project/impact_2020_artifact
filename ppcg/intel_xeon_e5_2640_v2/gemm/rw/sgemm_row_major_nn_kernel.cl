__kernel void kernel0(__global float *A, __global float *B, __global float *C)
{
    int b0 = get_group_id(0), b1 = get_group_id(1);
    int t0 = get_local_id(0), t1 = get_local_id(1);
    __local float shared_A[5][64];
    float private_C[5][2];

    {
      if (t1 <= 63)
        for (int c0 = 0; c0 <= 4; c0 += 1)
          shared_A[c0][t1] = A[(5 * b0 + c0) * 64 + t1];
      barrier(CLK_LOCAL_MEM_FENCE | CLK_GLOBAL_MEM_FENCE);
      if (142 * b1 + t1 <= 499) {
        private_C[0][0] = C[5 * b0 * 500 + (142 * b1 + t1)];
        if (t1 <= 49 && 142 * b1 + t1 <= 407)
          private_C[0][1] = C[5 * b0 * 500 + (142 * b1 + t1 + 92)];
        private_C[1][0] = C[(5 * b0 + 1) * 500 + (142 * b1 + t1)];
        if (t1 <= 49 && 142 * b1 + t1 <= 407)
          private_C[1][1] = C[(5 * b0 + 1) * 500 + (142 * b1 + t1 + 92)];
        private_C[2][0] = C[(5 * b0 + 2) * 500 + (142 * b1 + t1)];
        if (t1 <= 49 && 142 * b1 + t1 <= 407)
          private_C[2][1] = C[(5 * b0 + 2) * 500 + (142 * b1 + t1 + 92)];
        private_C[3][0] = C[(5 * b0 + 3) * 500 + (142 * b1 + t1)];
        if (t1 <= 49 && 142 * b1 + t1 <= 407)
          private_C[3][1] = C[(5 * b0 + 3) * 500 + (142 * b1 + t1 + 92)];
        private_C[4][0] = C[(5 * b0 + 4) * 500 + (142 * b1 + t1)];
        if (t1 <= 49 && 142 * b1 + t1 <= 407)
          private_C[4][1] = C[(5 * b0 + 4) * 500 + (142 * b1 + t1 + 92)];
        for (int c3 = 0; c3 <= 63; c3 += 1) {
          private_C[0][0] += (shared_A[0][c3] * B[c3 * 500 + (142 * b1 + t1)]);
          if (t1 <= 49 && 142 * b1 + t1 <= 407)
            private_C[0][1] += (shared_A[0][c3] * B[c3 * 500 + (142 * b1 + t1 + 92)]);
          private_C[1][0] += (shared_A[1][c3] * B[c3 * 500 + (142 * b1 + t1)]);
          if (t1 <= 49 && 142 * b1 + t1 <= 407)
            private_C[1][1] += (shared_A[1][c3] * B[c3 * 500 + (142 * b1 + t1 + 92)]);
          private_C[2][0] += (shared_A[2][c3] * B[c3 * 500 + (142 * b1 + t1)]);
          if (t1 <= 49 && 142 * b1 + t1 <= 407)
            private_C[2][1] += (shared_A[2][c3] * B[c3 * 500 + (142 * b1 + t1 + 92)]);
          private_C[3][0] += (shared_A[3][c3] * B[c3 * 500 + (142 * b1 + t1)]);
          if (t1 <= 49 && 142 * b1 + t1 <= 407)
            private_C[3][1] += (shared_A[3][c3] * B[c3 * 500 + (142 * b1 + t1 + 92)]);
          private_C[4][0] += (shared_A[4][c3] * B[c3 * 500 + (142 * b1 + t1)]);
          if (t1 <= 49 && 142 * b1 + t1 <= 407)
            private_C[4][1] += (shared_A[4][c3] * B[c3 * 500 + (142 * b1 + t1 + 92)]);
        }
        C[5 * b0 * 500 + (142 * b1 + t1)] = private_C[0][0];
        if (t1 <= 49 && 142 * b1 + t1 <= 407)
          C[5 * b0 * 500 + (142 * b1 + t1 + 92)] = private_C[0][1];
        C[(5 * b0 + 1) * 500 + (142 * b1 + t1)] = private_C[1][0];
        if (t1 <= 49 && 142 * b1 + t1 <= 407)
          C[(5 * b0 + 1) * 500 + (142 * b1 + t1 + 92)] = private_C[1][1];
        C[(5 * b0 + 2) * 500 + (142 * b1 + t1)] = private_C[2][0];
        if (t1 <= 49 && 142 * b1 + t1 <= 407)
          C[(5 * b0 + 2) * 500 + (142 * b1 + t1 + 92)] = private_C[2][1];
        C[(5 * b0 + 3) * 500 + (142 * b1 + t1)] = private_C[3][0];
        if (t1 <= 49 && 142 * b1 + t1 <= 407)
          C[(5 * b0 + 3) * 500 + (142 * b1 + t1 + 92)] = private_C[3][1];
        C[(5 * b0 + 4) * 500 + (142 * b1 + t1)] = private_C[4][0];
        if (t1 <= 49 && 142 * b1 + t1 <= 407)
          C[(5 * b0 + 4) * 500 + (142 * b1 + t1 + 92)] = private_C[4][1];
      }
    }
}
