#!/usr/bin/env bash

gcc -std=c99 sgemm_row_major_nn_host.c ocl_utilities.c -lOpenCL && \

echo "measuring runtime..." && \
./a.out $1 $2 && \
echo "ns"