__kernel void kernel0(__global float *A, __global float *B, __global float *C)
{
    int b0 = get_group_id(0), b1 = get_group_id(1);
    int t0 = get_local_id(0), t1 = get_local_id(1);
    __local float shared_B[256][2];
    float private_C[1][2];

    for (int c1 = 0; c1 <= 1023; c1 += 2) {
      private_C[0][0] = C[(16 * b0 + t0) * 1024 + c1];
      private_C[0][1] = C[(16 * b0 + t0) * 1024 + (c1 + 1)];
      for (int c2 = 0; c2 <= 1023; c2 += 256) {
        for (int c3 = t0; c3 <= 255; c3 += 16)
          for (int c4 = 0; c4 <= 1; c4 += 1)
            shared_B[c3][c4] = B[(c2 + c3) * 1024 + (c1 + c4)];
        barrier(CLK_LOCAL_MEM_FENCE | CLK_GLOBAL_MEM_FENCE);
        for (int c3 = 0; c3 <= 255; c3 += 1) {
          private_C[0][0] += (A[(16 * b0 + t0) * 1024 + (c2 + c3)] * shared_B[c3][0]);
          private_C[0][1] += (A[(16 * b0 + t0) * 1024 + (c2 + c3)] * shared_B[c3][1]);
        }
        barrier(CLK_LOCAL_MEM_FENCE | CLK_GLOBAL_MEM_FENCE);
      }
      C[(16 * b0 + t0) * 1024 + c1] = private_C[0][0];
      C[(16 * b0 + t0) * 1024 + (c1 + 1)] = private_C[0][1];
      barrier(CLK_LOCAL_MEM_FENCE | CLK_GLOBAL_MEM_FENCE);
    }
}
