#include <assert.h>
#include <limits.h>
#include <math.h>
#include <stdio.h>
#include "ocl_utilities.h"
int main(int argc, const char **argv) {
int platform_id = strtol(argv[1], NULL, 10);
int device_id = strtol(argv[2], NULL, 10);

    #define M_VAL 1024
    #define N_VAL 1024
    #define K_VAL 1024

    static float A[M_VAL][K_VAL];
    static float B[K_VAL][N_VAL];
    static float C[M_VAL][N_VAL];

    for (int i = 0; i < M_VAL * K_VAL; ++i) ((float *)A)[i] = (i % 10) + 1;
    for (int i = 0; i < K_VAL * N_VAL; ++i) ((float *)B)[i] = (i % 10) + 1;
    for (int i = 0; i < M_VAL * N_VAL; ++i) ((float *)C)[i] = 0;

    {
      #define openclCheckReturn(ret) \
  if (ret != CL_SUCCESS) {\
    fprintf(stderr, "OpenCL error: %s\n", opencl_error_string(ret)); \
    fflush(stderr); \
    assert(ret == CL_SUCCESS);\
  }

      cl_mem dev_A;
      cl_mem dev_B;
      cl_mem dev_C;
      
      cl_device_id device;
      cl_context context;
      cl_program program;
      cl_command_queue queue;
      cl_int err;
      device = opencl_create_device(platform_id, device_id);
      context = clCreateContext(NULL, 1, &device, NULL, NULL, &err);
      openclCheckReturn(err);
      queue = clCreateCommandQueue(context, device, CL_QUEUE_PROFILING_ENABLE, &err);
      openclCheckReturn(err);
      program = opencl_build_program_from_file(context, device, "sgemm_row_major_nn_kernel.cl", "");
      
      {
        dev_A = clCreateBuffer(context, CL_MEM_READ_WRITE, (1024) * (1024) * sizeof(float), NULL, &err);
        openclCheckReturn(err);
      }
      {
        dev_B = clCreateBuffer(context, CL_MEM_READ_WRITE, (1024) * (1024) * sizeof(float), NULL, &err);
        openclCheckReturn(err);
      }
      {
        dev_C = clCreateBuffer(context, CL_MEM_READ_WRITE, (1024) * (1024) * sizeof(float), NULL, &err);
        openclCheckReturn(err);
      }
      
      openclCheckReturn(clEnqueueWriteBuffer(queue, dev_A, CL_TRUE, 0, (1024) * (1024) * sizeof(float), A, 0, NULL, NULL));
      openclCheckReturn(clEnqueueWriteBuffer(queue, dev_B, CL_TRUE, 0, (1024) * (1024) * sizeof(float), B, 0, NULL, NULL));
      openclCheckReturn(clEnqueueWriteBuffer(queue, dev_C, CL_TRUE, 0, (1024) * (1024) * sizeof(float), C, 0, NULL, NULL));
      {
        size_t global_work_size[2] = {(64) * 16, (1) * 1};
        size_t block_size[2] = {16, 1};
        cl_kernel kernel0 = clCreateKernel(program, "kernel0", &err);
        openclCheckReturn(err);
        openclCheckReturn(clSetKernelArg(kernel0, 0, sizeof(cl_mem), (void *) &dev_A));
        openclCheckReturn(clSetKernelArg(kernel0, 1, sizeof(cl_mem), (void *) &dev_B));
        openclCheckReturn(clSetKernelArg(kernel0, 2, sizeof(cl_mem), (void *) &dev_C));
        unsigned long min_runtime = ULONG_MAX;
for (int i = 0; i < 10; ++i) {
    openclCheckReturn(clEnqueueWriteBuffer(queue, dev_A, CL_TRUE, 0, (1024) * (1024) * sizeof(float), A, 0, NULL, NULL));
openclCheckReturn(clEnqueueWriteBuffer(queue, dev_B, CL_TRUE, 0, (1024) * (1024) * sizeof(float), B, 0, NULL, NULL));
openclCheckReturn(clEnqueueWriteBuffer(queue, dev_C, CL_TRUE, 0, (1024) * (1024) * sizeof(float), C, 0, NULL, NULL));
    cl_event event;
    openclCheckReturn(clEnqueueNDRangeKernel(queue, kernel0, 2, NULL, global_work_size, block_size, 0, NULL, &event));
    openclCheckReturn(clFinish(queue));
    cl_ulong start, end;
    openclCheckReturn(clGetEventProfilingInfo(event, CL_PROFILING_COMMAND_START, sizeof(cl_ulong), &start, NULL));
    openclCheckReturn(clGetEventProfilingInfo(event, CL_PROFILING_COMMAND_END, sizeof(cl_ulong), &end, NULL));
    unsigned long runtime = end - start;
    if (runtime < min_runtime) min_runtime = runtime;
}
min_runtime = ULONG_MAX;
for (int i = 0; i < 200; ++i) {
    openclCheckReturn(clEnqueueWriteBuffer(queue, dev_A, CL_TRUE, 0, (1024) * (1024) * sizeof(float), A, 0, NULL, NULL));
openclCheckReturn(clEnqueueWriteBuffer(queue, dev_B, CL_TRUE, 0, (1024) * (1024) * sizeof(float), B, 0, NULL, NULL));
openclCheckReturn(clEnqueueWriteBuffer(queue, dev_C, CL_TRUE, 0, (1024) * (1024) * sizeof(float), C, 0, NULL, NULL));
    cl_event event;
    openclCheckReturn(clEnqueueNDRangeKernel(queue, kernel0, 2, NULL, global_work_size, block_size, 0, NULL, &event));
    openclCheckReturn(clFinish(queue));
    cl_ulong start, end;
    openclCheckReturn(clGetEventProfilingInfo(event, CL_PROFILING_COMMAND_START, sizeof(cl_ulong), &start, NULL));
    openclCheckReturn(clGetEventProfilingInfo(event, CL_PROFILING_COMMAND_END, sizeof(cl_ulong), &end, NULL));
    unsigned long runtime = end - start;
    if (runtime < min_runtime) min_runtime = runtime;
}
printf("%lu", min_runtime);
        openclCheckReturn(clReleaseKernel(kernel0));
        clFinish(queue);
      }
      
      openclCheckReturn(clEnqueueReadBuffer(queue, dev_C, CL_TRUE, 0, (1024) * (1024) * sizeof(float), C, 0, NULL, NULL));
      openclCheckReturn(clReleaseMemObject(dev_A));
      openclCheckReturn(clReleaseMemObject(dev_B));
      openclCheckReturn(clReleaseMemObject(dev_C));
      openclCheckReturn(clReleaseCommandQueue(queue));
      openclCheckReturn(clReleaseProgram(program));
      openclCheckReturn(clReleaseContext(context));
    }

#ifdef WRITE_GOLD
    WRITE_GOLD
#endif
}
